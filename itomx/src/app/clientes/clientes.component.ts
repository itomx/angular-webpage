import { Component, OnInit } from '@angular/core';
import { PostsService } from '../services/posts.service';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {

  constructor(private postsService: PostsService) { }

  posts = {}
  ngOnInit(): void {
    console.log("Componente de cliente se inicio");
  }

}
