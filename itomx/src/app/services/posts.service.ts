import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class PostsService {
    constructor(private http: HttpClient){}

    getPosts(){
        return this.http.get('https://jsonplaceholder.typicode.com/posts');
    }

    getPost(postId: String){
        return this.http.get('https://jsonplaceholder.typicode.com/posts/' + postId);
    }
}