import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ClientesComponent } from './clientes/clientes.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

import { MatButtonModule } from '@angular/material/button'
import { MatInputModule } from '@angular/material/input'
import { MatFormFieldModule } from '@angular/material/form-field'

import { PostsService } from './services/posts.service';
import { HttpClientModule } from '@angular/common/http';
import { MatCardModule } from '@angular/material/card';
import { AppRoutingModule } from './app-routing.module';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { PolizasComponent } from './polizas/polizas.component';
import { AutosComponent } from './autos/autos.component';
import { SegurosComponent } from './seguros/seguros.component';

@NgModule({
  declarations: [
    UsuariosComponent,
    PolizasComponent,
    AutosComponent,
    SegurosComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    AppRoutingModule,
    MatInputModule,
    MatFormFieldModule,
    HttpClientModule,
    MatCardModule
  ],
  providers: [PostsService],
  bootstrap: [UsuariosComponent]
})
export class AppModule { }
