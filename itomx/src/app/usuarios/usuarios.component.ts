import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { PostsService } from '../services/posts.service';

@Component({
    selector: 'usuarios',
    templateUrl : './usuarios.component.html',
    styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit{

    constructor(private postsService: PostsService){}

    posts = {}
    ngOnInit(){
        console.log("Component de usuarios se inicio")
        this.postsService.getPosts().subscribe((response) => this.posts = response);
    }

    campoName = new FormControl("")
    campoLastName = new FormControl("")
    campoAge = new FormControl("")
    postID = new FormControl("")
    name = "Hugo Vazquez"
    user = {}
    actualPost = {}
    actualPostId = 0;
    content = ''
    userId = 0
    title = ''

    imprimirNombre(){
        let user = {
            name : this.campoName.value,
            lastname: this.campoLastName.value,
            age: this.campoAge.value
        }
        console.log(this.postID.value);
        this.postsService.getPost(this.postID.value).subscribe((response) => {
            this.construirPosts(response);
        });
    }

    construirPosts(post: any){
        this.actualPost = JSON.parse(JSON.stringify(post));
            console.log(this.actualPost);// 2
            this.actualPostId = JSON.parse(JSON.stringify(post))["id"];
            this.content = JSON.parse(JSON.stringify(post))["body"];
            this.userId = JSON.parse(JSON.stringify(post))["userId"];
            this.title = JSON.parse(JSON.stringify(post))["title"];
    }
}