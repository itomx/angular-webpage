import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PagosComponent } from './pagos/pagos.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { MatCardModule } from '@angular/material/card';
import { BrowserModule } from '@angular/platform-browser';
import { ClientesComponent } from './clientes/clientes.component';
import { AutosComponent } from './autos/autos.component';

const routes: Routes = [
{ path: '', component: ClientesComponent},
{ path: 'pagos', component: PagosComponent},
{ path: 'autos', component: AutosComponent},
{ path: 'usuarios', component: UsuariosComponent}
];

@NgModule({
  imports: [BrowserModule,MatCardModule, RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
